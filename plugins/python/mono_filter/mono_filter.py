from krita import *
from PyQt5.QtWidgets import QMessageBox
from .ui_text import ui_text


def clamp(x: int):
    if x < 0:
        return 0
    elif x > 255:
        return 255
    return x


class MyExtension(Extension):

    def __init__(self, parent):
        super().__init__(parent)
        self.msgbox = QMessageBox()
        self.app = Krita.instance()
        self.doc = None
        self.node = None
        self.root = None

    def setup(self):
        pass

    def createActions(self, window):
        action = window.createAction(
            'error_diffusion', ui_text['filter.diffusion-dither'], "filter")
        action.triggered.connect(self.diffusion_dither)

    def get_doc_node_root(self):
        active_doc = Krita.instance().activeDocument()
        if active_doc is None:
            self.msgbox.warning(None, '', ui_text['warning.no-document'])
            return False

        active_node = active_doc.activeNode()
        if active_node.type() != 'paintlayer':
            self.msgbox.warning(None, '', ui_text['warning.no-paintlayer'])
            return False

        self.doc = active_doc
        self.node = active_node
        self.root = active_doc.rootNode()
        return True

    def create_new_node(self):
        height = self.doc.height()
        width = self.doc.width()
        new_node = self.doc.createNode(self.node.name()+' [MF]', 'paintlayer')
        new_node.setPixelData(self.node.pixelData(
            0, 0, width, height), 0, 0, width, height)
        self.root.addChildNode(new_node, None)
        self.doc.refreshProjection()

        return new_node, width, height

    def diffusion_dither(self):

        if not self.get_doc_node_root():
            return
        new_node, width, height = self.create_new_node()
        bitmap = [[0 for i in range(height)] for j in range(width)]

        for w in range(width):
            for h in range(height):
                pixel = new_node.pixelData(w, h, 1, 1)
                brightness = (ord(pixel[0])*117+ord(pixel[1])
                              * 601+ord(pixel[2])*306) >> 10
                bitmap[w][h] = brightness

        for w in range(1, width-1):
            for h in range(height-1):
                old = bitmap[w][h]
                new = round(old / 255) * 255
                bitmap[w][h] = new
                quant_error = old - new
                bitmap[w+1][h] = clamp(bitmap[w+1][h] + quant_error * 7//16)
                bitmap[w-1][h+1] = clamp(bitmap[w-1][h+1] + quant_error * 3//16)
                bitmap[w][h+1] = clamp(bitmap[w][h+1] + quant_error * 5//16)
                bitmap[w+1][h+1] = clamp(bitmap[w+1][h+1] + quant_error * 1//16)

        for w in range(width):
            for h in range(height):
                if bitmap[w][h] >= 128:
                    new_node.setPixelData(bytes([255, 255, 255, 255]), w, h, 1, 1)
                else:
                    new_node.setPixelData(bytes([0, 0, 0, 255]), w, h, 1, 1)

        self.doc.refreshProjection()


Krita.instance().addExtension(MyExtension(Krita.instance()))
