from locale import getdefaultlocale

system_language = getdefaultlocale()[0]

LANG_ZH_CN = {
    'warning.no-document': '无法执行\n请打开一个文档。',
    'warning.no-paintlayer': '无法执行\n请选中一个颜料图层。',
    'filter.diffusion-dither': '扩散仿色'
}

LANG_EN = {
    'warning.no-document': 'Cannot apply\nPlease open a document.',
    'warning.no-paintlayer': 'Cannot apply\nPlease select a paint layer.',
    'filter.diffusion-dither': 'Diffusion Dither'
}

if 'zh' in system_language:
    ui_text = LANG_ZH_CN
else:
    ui_text = LANG_EN
